﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitSkoleni2Shared
{
    public class Car
    {
        public string Name { get; set; }

        public string Color { get; set; }

        public string Cena { get; set; }
    }
}
